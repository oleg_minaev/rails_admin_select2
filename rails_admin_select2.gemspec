# -*- encoding: utf-8 -*-
# stub: rails_admin_select2 0.0.3.pre ruby lib

Gem::Specification.new do |s|
  s.name = "rails_admin_select2"
  s.version = "0.0.3.pre"

  s.required_rubygems_version = Gem::Requirement.new("> 1.3.1") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Sebastian Gassner"]
  s.date = "2014-07-26"
  s.description = "A custom field providing select2 support for rails_admin."
  s.email = ["sebastian.gassner@gmail.com"]
  s.files = ["MIT-LICENSE", "Rakefile", "app/views", "app/views/rails_admin", "app/views/rails_admin/main", "app/views/rails_admin/main/_form_select2.html.erb", "app/views/rails_admin/main/_form_select2_tags.html.erb", "lib/rails_admin_select2", "lib/rails_admin_select2.rb", "lib/rails_admin_select2/engine.rb", "lib/rails_admin_select2/version.rb"]
  s.homepage = "https://github.com/dieunb/rails_admin_select2"
  s.rubygems_version = "2.4.1"
  s.summary = "Select2 for rails_admin"

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<rails_admin>, ["~> 0.6.0"])
      s.add_runtime_dependency(%q<select2-rails>, [">= 0"])
    else
      s.add_dependency(%q<rails_admin>, ["~> 0.6.0"])
      s.add_dependency(%q<select2-rails>, [">= 0"])
    end
  else
    s.add_dependency(%q<rails_admin>, ["~> 0.6.0"])
    s.add_dependency(%q<select2-rails>, [">= 0"])
  end
end
